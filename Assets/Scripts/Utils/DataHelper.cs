﻿using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataHelper : MonoBehaviour
{
    public static T ReadData<T>(string path)
    {
        path = Application.persistentDataPath + "/" + path;
        if (File.Exists(path))
        {
            string fileContents = File.ReadAllText(path);
            return JsonUtility.FromJson<T>(fileContents);
        }

        return default;
    }

    public static void SaveData<T>(string path, T data)
    {
        path = Application.persistentDataPath + "/" + path;
        string jsonString = JsonUtility.ToJson(data);

        File.WriteAllText(path, jsonString);
    }
}