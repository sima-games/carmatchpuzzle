﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace CarConstruct.Utils
{
    public static class Constants
    {
        public static readonly float CarMovementDuration = 0.5f;
        public static readonly float UIFadeDuration = 0.4f;
        public static readonly float PuzzleSpawnWidth = 60f;
        public static readonly float LevelTruckSpawnWidth = 60f;
        public static readonly float LevelTruckSpawnHeight = 30f;
        public static readonly float MenuLevelGridCells = 3;
        public static readonly float MenuLevelGridRows = 3;
        public static readonly string SaveData = Application.persistentDataPath + "/gamedata.json";
    }
}
