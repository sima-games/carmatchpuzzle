﻿using Assets.Scripts.Utils;
using CarConstruct.Utils;
using DG.Tweening;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;

namespace CarConstruct.UI
{
    public class FadeCanvas : MonoBehaviour
    {
        [SerializeField] private UnityEvent _onFadeInComplete;
        [SerializeField] private UnityEvent _onFadeOutComplete;
        [SerializeField] private CanvasGroup _canvasGroup;
        [SerializeField] private SceneLoader _sceneLoader;
        private void Start() => FadeIn();

        public void FadeIn()
        {
            _canvasGroup.alpha = 1f;
            _canvasGroup.DOFade(0f, Constants.UIFadeDuration).SetEase(Ease.Linear).OnComplete(() => _onFadeInComplete?.Invoke());
        }

        public void FadeOut()
        {
            _canvasGroup.alpha = 0f;
            _canvasGroup.DOFade(1f, Constants.UIFadeDuration).SetEase(Ease.Linear).OnComplete(() => _onFadeOutComplete?.Invoke());
        }
        
        public void FadeOutToScene(string scene)
        {
            _canvasGroup.alpha = 0f;
            _canvasGroup.DOFade(1f, Constants.UIFadeDuration).SetEase(Ease.Linear).OnComplete(() => 
            {
                _onFadeOutComplete?.Invoke();
                _sceneLoader.LoadScene(scene);
            });
        }
    }
}
