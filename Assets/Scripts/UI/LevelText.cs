﻿using CarConstruct.Utils;
using DG.Tweening;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace CarConstruct.UI
{
    public class LevelText : MonoBehaviour
    {
        [SerializeField] private Text _text = null;
        public void SetLevel(Tuple<int, bool> tuple) => _text.text = $"УРОВЕНЬ {tuple.Item1 + 1}";
    }
}
