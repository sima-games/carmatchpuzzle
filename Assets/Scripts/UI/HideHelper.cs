﻿using DG.Tweening;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;

namespace Assets.Scripts.UI
{
    class HideHelper : MonoBehaviour
    {
        [SerializeField] private Vector2 _hidePos;
        [SerializeField] private float _hideDuration = 0;
        [SerializeField] private Ease _hideEase = Ease.OutElastic;

        [SerializeField] private Vector2 _showPos;
        [SerializeField] private float _showDuration = 0;
        [SerializeField] private Ease _showEase = Ease.InElastic;

        [SerializeField] private UnityEvent _onShowed;
        [SerializeField] private UnityEvent _onHidden;

        public void Hide() => transform?.DOMove(new Vector2(transform.position.x + _hidePos.x, transform.position.y + _hidePos.y), _hideDuration).SetEase(_hideEase).OnComplete(() => _onHidden?.Invoke());
        public void Show() => transform?.DOMove(new Vector2(_showPos.x, _showPos.y), _showDuration).SetEase(_showEase).OnComplete(() => _onShowed?.Invoke());
    }
}
