﻿using CarConstruct;
using CarConstruct.StaticData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI
{
    class ExampleCar : MonoBehaviour
    {
        [SerializeField] private Image image;
        private void Start()
        {
            image.sprite = DefaultModeStaticData.CurrentCarData.ExampleCar;
        }
    }
}
