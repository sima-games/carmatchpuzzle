﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Assets.Scripts.PuzzlePieces
{
    class ZRotationHandler : MonoBehaviour, IPointerClickHandler
    {
        [SerializeField] private float _zDegrees;
        public void OnPointerClick(PointerEventData eventData)
        {
            transform.parent.Rotate(0, 0, _zDegrees);
        }
    }
}
