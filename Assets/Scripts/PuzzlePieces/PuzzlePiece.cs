﻿using CarConstruct.Scenes;
using CarConstruct.ScriptableObjects;
using CarConstruct.Truck;
using CarConstruct.Utils;
using DG.Tweening;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;

namespace CarConstruct.PuzzlePieces
{
    public class PuzzlePiece : MonoBehaviour
    {
        public bool Connected { get; private set; }

        [SerializeField] private SpriteRenderer _spriteRenderer;
        [SerializeField] private CircleCollider2D _circleCollider2D;
        [SerializeField] private float _puzzleOffset;
        [SerializeField] private float _bindDistance;
        private PuzzlePieceData _puzzlePieceData;
        private Vector3 _offset;
        private Camera _mainCamera;
        private GameScene _gameScene;
        private GameObject _truck;
        [SerializeField] protected AudioSource _audio;
        [SerializeField] protected AudioClip _startAudio;
        private void Start()
        {
            _mainCamera = Camera.main;
        }

        public void InitPiece(GameObject truck, float amount, Vector2 initPos, int indexPos, GameScene gameScene, PuzzlePieceData data, Sprite sprite, bool byWidth = true)
        {
            _truck = truck;
            _gameScene = gameScene;
            _puzzlePieceData = data;
            _spriteRenderer.sprite = sprite;
            float x, y;
            if (byWidth)
            {
                x = Constants.PuzzleSpawnWidth / (amount * 2) + Constants.PuzzleSpawnWidth / amount * indexPos - Constants.PuzzleSpawnWidth / 2;
                y = initPos.y;
            } 
            else
            {
                x = initPos.x + (indexPos % 2 * (_circleCollider2D.radius + _puzzleOffset));
                y = initPos.y - (indexPos / 2 * (_circleCollider2D.radius + _puzzleOffset));
            }
            transform.position = new Vector2(x, y);
            transform.localScale = data.Scale;

            float[] randomZRotations = { 0, 90, -90, 180 };
            //transform.rotation = Quaternion.Euler(_puzzlePieceData.AcceptableRotations[0]);
            transform.rotation = Quaternion.Euler(0, 0, randomZRotations[UnityEngine.Random.Range(0, randomZRotations.Length)]);
            _spriteRenderer.flipX = data.FlipX;
            _spriteRenderer.flipY = data.FlipY;
        }

        private void OnMouseDrag()
        {
            UpdatePosition(_mainCamera.ScreenToWorldPoint(Input.mousePosition));
        }

        private void OnMouseDown()
        {
            var mousePos = _mainCamera.ScreenToWorldPoint(Input.mousePosition);
            _offset = transform.position - mousePos;

            UpdatePosition(mousePos);
        }

        private void OnMouseUp()
        {
            /*_puzzlePieceData.Sprite = _spriteRenderer.sprite;
            _puzzlePieceData.Position = transform.position;
            _puzzlePieceData.FlipX = _spriteRenderer.flipX;
            _puzzlePieceData.FlipY = _spriteRenderer.flipY;
            EditorUtility.SetDirty(_puzzlePieceData);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();*/
            float distance = Vector2.Distance(transform.position, _puzzlePieceData.Position);
            if (distance > _bindDistance) return;
            
            if (_puzzlePieceData.AcceptAllRotations)
            {
                BindPuzzlePiece();
            } 
            else
            {
                foreach (var rotation in _puzzlePieceData.AcceptableRotations)
                {                    
                    if (transform.eulerAngles == rotation)
                    {
                        BindPuzzlePiece();
                        return;
                    }
                }
            }

            void BindPuzzlePiece()
            {
                _audio.PlayOneShot(_startAudio);
                transform.parent = _truck.transform;
                transform.position = _puzzlePieceData.Position;
                _circleCollider2D.enabled = false;
                Connected = true;
                foreach (Transform child in transform)
                {
                    Destroy(child.gameObject);
                }
                _gameScene.CheckPuzzles();
            }
        }

        private void UpdatePosition(Vector3 position)
        {
            transform.position = position + _offset;
        }
    }
}
