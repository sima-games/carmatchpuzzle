﻿using CarConstruct.Utils;
using DG.Tweening;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace CarConstruct.Truck
{
    public class MenuTruck : BasicTruck, IPointerClickHandler
    {
        [SerializeField] private UnityEvent _onTruckClicked = null;

        private void Start() {
            _startPosition = transform.position.x;
        }

        public void MoveAndChangeOutfit(Sprite sprite, bool forward)
        {
            var destination = _maxRightPosition;
            var returnPosition = _maxLeftPosition;
            if (forward)
            {
                destination = _maxLeftPosition;
                returnPosition = _maxRightPosition;
            }

            _onTruckMovementStarted?.Invoke();

            transform?.DOMoveX(destination, Constants.CarMovementDuration).SetEase(Ease.InFlash).OnComplete(() =>
            {
                transform.position = new Vector2(returnPosition, transform.position.y);
                SetOutfit(sprite);
                transform?.DOMoveX(_startPosition, Constants.CarMovementDuration).SetEase(Ease.OutFlash).OnComplete(() => _onTruckMoved.Invoke());
            });
        }

        public void SetOutfit(Sprite sprite)
        {
            _spriteRenderer.sprite = sprite;
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            _onTruckClicked?.Invoke();

            MoveToNextScene();
        }
    }
}
