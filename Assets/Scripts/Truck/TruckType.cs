﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarConstruct.Truck
{
    public enum TruckType
    {
        Basic,
        ExpertBlue,
        ExpertRed,
        MasterBlue,
        MasterRed,
        MasterViolet,
    }
}
