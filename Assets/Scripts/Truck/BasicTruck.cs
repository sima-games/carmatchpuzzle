﻿using CarConstruct.StaticData;
using CarConstruct.Utils;
using DG.Tweening;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace CarConstruct.Truck
{
    public class BasicTruck : MonoBehaviour
    {
        public SpriteRenderer SpriteRenderer => _spriteRenderer;

        [SerializeField] protected SpriteRenderer _spriteRenderer = null;
        [SerializeField] protected float _maxRightPosition = 0;
        [SerializeField] protected float _maxLeftPosition = 0;
        [SerializeField] protected UnityEvent _onTruckMovementStarted = null;
        [SerializeField] protected UnityEvent _onTruckMovedToNextScene = null;
        [SerializeField] protected UnityEvent _onTruckMovedToPreviousScene = null;
        [SerializeField] protected UnityEvent _onTruckMoved = null;
        [SerializeField] protected TruckType _truckType = TruckType.Basic;
        [SerializeField] protected float _carMovementDuration = 0.5f;
        [SerializeField] protected Ease _toCenterEase = Ease.OutFlash;
        [SerializeField] protected Ease _toSceneEase = Ease.InFlash;

        protected float _startPosition = 0;

        private void Start()
        {
            Sprite sprite = null;
            switch (_truckType)
            {
                case TruckType.Basic: 
                    sprite = DefaultModeStaticData.CurrentLevelData.CarTemplate;
                    break;
                case TruckType.ExpertBlue:
                    sprite = ExpertModeStaticData.CurrentExpertLevelData.CarTemplate;
                    break;
                case TruckType.ExpertRed:
                    sprite = ExpertModeStaticData.CurrentExpertLevelData.SecondCarTemplate;
                    break;
                case TruckType.MasterBlue:
                    sprite = MasterModeStaticData.CurrentMasterLevelData.CarTemplate;
                    break;
                case TruckType.MasterRed:
                    sprite = MasterModeStaticData.CurrentMasterLevelData.SecondCarTemplate;
                    break;
                case TruckType.MasterViolet:
                    sprite = MasterModeStaticData.CurrentMasterLevelData.ThirdCarTemplate;
                    break;
            }
            _spriteRenderer.sprite = sprite;
            _startPosition = transform.position.x;
        }

        public void MoveToNextScene()
        {
            _onTruckMovementStarted?.Invoke();

            transform?.DOMoveX(_maxRightPosition, _carMovementDuration).SetEase(_toSceneEase).OnComplete(() =>
            {
                _onTruckMovedToNextScene?.Invoke();
            });
        }
        
        public void MoveToCenterPosition()
        {
            _onTruckMovementStarted?.Invoke();

            transform?.DOMoveX(_startPosition, _carMovementDuration).SetEase(_toCenterEase).OnComplete(() =>
            {
                _onTruckMoved?.Invoke();
            });
        }

        public void MoveToStartPosition()
        {
            _onTruckMovementStarted?.Invoke();

            transform?.DOMoveX(_maxLeftPosition, _carMovementDuration).SetEase(_toSceneEase).OnComplete(() =>
            {
                _onTruckMovedToPreviousScene?.Invoke();
            });
        }
    }
}
