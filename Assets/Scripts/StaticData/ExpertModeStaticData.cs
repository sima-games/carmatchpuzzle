﻿using CarConstruct.ScriptableObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarConstruct.StaticData
{
    class ExpertModeStaticData : GeneralStaticData
    {
        public static Tuple<CarData, CarData> NextTwoCarsToUnlock;
        public static Tuple<CarData, CarData> CurrentTwoCarsData = null;

        public void SetCurrentCarData(Tuple<CarData, CarData> carData)
        {
            CurrentTwoCarsData = carData;
        }
    }
}
