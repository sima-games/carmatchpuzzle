﻿using CarConstruct.ScriptableObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarConstruct.StaticData
{
    class DefaultModeStaticData : GeneralStaticData
    {
        public static CarData NextCarToUnlock;
        public static CarData CurrentCarData = null;

        public void SetCurrentCarData(CarData carData)
        {
            CurrentCarData = carData;
        }
    }
}
