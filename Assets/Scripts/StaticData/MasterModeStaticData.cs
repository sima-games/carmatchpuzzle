﻿using CarConstruct.ScriptableObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarConstruct.StaticData
{
    class MasterModeStaticData : GeneralStaticData
    {
        public static Tuple<CarData, CarData, CarData> NextThreeCarsToUnlock;
        public static Tuple<CarData, CarData, CarData> CurrentThreeCarsData = null;

        public void SetCurrentCarData(Tuple<CarData, CarData, CarData> carData)
        {
            CurrentThreeCarsData = carData;
        }
    }
}