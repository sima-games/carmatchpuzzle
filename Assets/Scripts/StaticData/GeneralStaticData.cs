﻿using CarConstruct.ScriptableObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;

namespace CarConstruct.StaticData
{
    class GeneralStaticData : MonoBehaviour
    {
        [SerializeField] private UnityEvent _startEvent;
        public void Start()
        {
            _startEvent?.Invoke();
        }

        public static LevelData CurrentLevelData = null;
        public static ExpertLevelData CurrentExpertLevelData = null;
        public static MasterLevelData CurrentMasterLevelData = null;

        public void SetCurrentLevelData(LevelData levelData)
        {
            CurrentLevelData = levelData;
        }
        
        public void SetCurrentLevelData(ExpertLevelData levelData)
        {
            CurrentExpertLevelData = levelData;
        }
        
        public void SetCurrentLevelData(MasterLevelData levelData)
        {
            CurrentMasterLevelData = levelData;
        }
    }
}
