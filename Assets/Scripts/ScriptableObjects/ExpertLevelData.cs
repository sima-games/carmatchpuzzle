﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace CarConstruct.ScriptableObjects
{
    [CreateAssetMenu(fileName = "expert_level_n", menuName = "CarConstruct/LevelData/ExpertLevelData", order = 10)]
    class ExpertLevelData : LevelData
    {
        [SerializeField] private Sprite _secondCarTemplate = null;
        [SerializeField] private CarData[] _secondCarDatas = null;
        public Sprite SecondCarTemplate => _secondCarTemplate;
        public CarData[] SecondCarDatas => _secondCarDatas;
    }
}
