﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace CarConstruct.ScriptableObjects
{
    [CreateAssetMenu(fileName = "master_level_n", menuName = "CarConstruct/LevelData/MasterLevelData", order = 10)]
    class MasterLevelData : ExpertLevelData
    {
        [SerializeField] private Sprite _thirdCarTemplate = null;
        [SerializeField] private CarData[] _thirdCarDatas = null;
        public Sprite ThirdCarTemplate => _thirdCarTemplate;
        public CarData[] ThirdCarDatas => _thirdCarDatas;
    }
}
