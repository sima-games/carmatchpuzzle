﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace CarConstruct.ScriptableObjects
{
    [CreateAssetMenu(fileName = "puzzle_n_n", menuName = "CarConstruct/PuzzlePieceData", order = 10)]

    public class PuzzlePieceData : ScriptableObject
    {
        [SerializeField] private Sprite _sprite;
        [SerializeField] private Vector2 _position;
        [SerializeField] private Vector2 _scale = Vector2.one;
        [SerializeField] private Vector3[] _acceptableRotations = new Vector3[] {
            Vector3.zero,
        };
        [SerializeField] private bool _acceptAllRotations;
        [SerializeField] private bool _flipX;
        [SerializeField] private bool _flipY;
        public Sprite Sprite { get => _sprite; set => _sprite = value; }
        public Vector2 Position { get => _position; set => _position = value; }
        public Vector2 Scale { get => _scale; set => _scale = value; }
        public Vector3[] AcceptableRotations => _acceptableRotations;
        public bool AcceptAllRotations => _acceptAllRotations;
        public bool FlipX { get => _flipX; set => _flipX = value; }
        public bool FlipY { get => _flipY; set => _flipY = value; }
    }
}
