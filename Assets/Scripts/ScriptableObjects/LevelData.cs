﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace CarConstruct.ScriptableObjects
{
    [CreateAssetMenu(fileName = "level_n", menuName = "CarConstruct/LevelData/LevelData", order = 10)]
    public class LevelData : ScriptableObject
    {
        public SaveableLevelData SaveData;
        [SerializeField] private Sprite _carTemplate = null;
        [SerializeField] private CarData[] _carDatas = null;
        public Sprite CarTemplate => _carTemplate;
        public CarData[] CarDatas => _carDatas;
    }

    [Serializable]
    public class SaveableLevelData
    {
        public bool Locked = false;
    }
}