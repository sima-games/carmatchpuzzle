﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace CarConstruct.ScriptableObjects
{
    [CreateAssetMenu(fileName = "car_n", menuName = "CarConstruct/CarData", order = 10)]
    public class CarData : ScriptableObject
    {
        public SaveableCarData SaveData;
        [SerializeField] private int _level = 0;
        [SerializeField] private Sprite _exampleCar = null;
        [SerializeField] private PuzzlePieceData[] _puzzlePieceDatas = null;
        public int Level => _level;
        public Sprite ExampleCar => _exampleCar;
        public PuzzlePieceData[] PuzzlePieceDatas => _puzzlePieceDatas;
    }

    [Serializable]
    public class SaveableCarData
    {
        public bool Passed = false;
        public bool Locked = true;
    }
}