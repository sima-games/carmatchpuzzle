﻿using CarConstruct.Events;
using CarConstruct.ScriptableObjects;
using CarConstruct.StaticData;
using CarConstruct.Truck;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace CarConstruct.Scenes
{
    class MasterMenuScene : MonoBehaviour
    {
        [SerializeField] protected Image _star;
        [SerializeField] protected MenuTruck _truck;
        [SerializeField] private MenuTruck _secondTruck;
        [SerializeField] private MenuTruck _thirdTruck;
        [SerializeField] protected LevelSelector _levelSelector = null;
        [SerializeField] private UnityThreeCarsDataEvent _onLevelUpdated;
        private List<Tuple<CarData, CarData, CarData>> _availableCarDatas = new List<Tuple<CarData, CarData, CarData>>();
        private static bool isPlayed = false;
        [SerializeField] protected AudioSource _audio;
        [SerializeField] protected AudioClip _startAudio;
        private void Start()
        {
            var currentLevel = GeneralStaticData.CurrentMasterLevelData;
            var carDatas = currentLevel.CarDatas;
            var secondCarDatas = currentLevel.SecondCarDatas;
            var thirdCarDatas = currentLevel.ThirdCarDatas;
            for (int i = 0; i < carDatas.Length; i++)
            {
                var loadedData = DataHelper.ReadData<SaveableCarData>(carDatas[i].name + ".data");
                var secondLoadedData = DataHelper.ReadData<SaveableCarData>(secondCarDatas[i].name + ".data");
                var thirdLoadedData = DataHelper.ReadData<SaveableCarData>(thirdCarDatas[i].name + ".data");
                if (loadedData != null)
                {
                    carDatas[i].SaveData = loadedData;
                    secondCarDatas[i].SaveData = secondLoadedData;
                    thirdCarDatas[i].SaveData = thirdLoadedData;
                }

                if (!carDatas[i].SaveData.Locked && !secondCarDatas[i].SaveData.Locked && !thirdCarDatas[i].SaveData.Locked)
                {
                    _availableCarDatas.Add(new Tuple<CarData, CarData, CarData>(carDatas[i], secondCarDatas[i], thirdCarDatas[i]));
                }
            }

            var nextCarIndex = _availableCarDatas.LastIndexOf(_availableCarDatas.Last()) + 1;
            if (carDatas.Length > nextCarIndex)
            {
                MasterModeStaticData.NextThreeCarsToUnlock = new Tuple<CarData, CarData, CarData>(carDatas[nextCarIndex], secondCarDatas[nextCarIndex], thirdCarDatas[nextCarIndex]);
            }

            _levelSelector.NumberOfLevels = _availableCarDatas.Count;

            var startCars = _availableCarDatas[0];
            _truck.SetOutfit(startCars.Item1.ExampleCar);
            _secondTruck.SetOutfit(startCars.Item2.ExampleCar);
            _thirdTruck.SetOutfit(startCars.Item3.ExampleCar);
            _star.gameObject.SetActive(startCars.Item1.SaveData.Passed);
            _onLevelUpdated?.Invoke(startCars);

            if (!isPlayed)
            {
                isPlayed = true;
                _audio.PlayOneShot(_startAudio);
            }
        }

        public void UpdateLevel(Tuple<int, bool> tuple)
        {
            
            var levelIndex = tuple.Item1;
            var next = tuple.Item2;
            Debug.Log(_availableCarDatas[levelIndex].Item1.name);
            _star.gameObject.SetActive(_availableCarDatas[levelIndex].Item1.SaveData.Passed);

            _truck.MoveAndChangeOutfit(_availableCarDatas[levelIndex].Item1.ExampleCar, next);
            _secondTruck.MoveAndChangeOutfit(_availableCarDatas[levelIndex].Item2.ExampleCar, next);
            _thirdTruck.MoveAndChangeOutfit(_availableCarDatas[levelIndex].Item3.ExampleCar, next);
            _onLevelUpdated?.Invoke(_availableCarDatas[levelIndex]);
        }
    }
}
