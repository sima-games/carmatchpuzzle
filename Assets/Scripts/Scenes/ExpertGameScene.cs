﻿using CarConstruct.PuzzlePieces;
using CarConstruct.ScriptableObjects;
using CarConstruct.StaticData;
using CarConstruct.Truck;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;

namespace CarConstruct.Scenes
{
    public class ExpertGameScene : GameScene
    {
        private CarData _secondCarData;
        [SerializeField] private GameObject _secondTruck;

        private static bool isPlayed = false;

        private void Start()
        {
            _puzzles = new List<PuzzlePiece>();

            _carData = ExpertModeStaticData.CurrentTwoCarsData.Item1;
            _secondCarData = ExpertModeStaticData.CurrentTwoCarsData.Item2;
            var index = 0;
            var arraysLength = _carData.PuzzlePieceDatas.Length + _secondCarData.PuzzlePieceDatas.Length;
            foreach (var puzzle in _carData.PuzzlePieceDatas)
            {
                var puzzlePieceObject = Instantiate(_puzzlePiecePrefab);
                var puzzlePiece = puzzlePieceObject.GetComponent<PuzzlePiece>();
                puzzlePiece.InitPiece(_truck, arraysLength, _puzzleInitPos, index, this, puzzle, puzzle.Sprite);
                _puzzles.Add(puzzlePiece);
                index++;
            }

            foreach (var puzzle in _secondCarData.PuzzlePieceDatas)
            {
                var puzzlePieceObject = Instantiate(_puzzlePiecePrefab);
                var puzzlePiece = puzzlePieceObject.GetComponent<PuzzlePiece>();
                puzzlePiece.InitPiece(_secondTruck, arraysLength, _puzzleInitPos, index, this, puzzle, puzzle.Sprite);
                _puzzles.Add(puzzlePiece);
                index++;
            }

            if(!isPlayed)
            {
                isPlayed = true;
                _audio.PlayOneShot(_startAudio);
            }
        }

        public override void CheckPuzzles()
        {
            foreach (var puzzle in _puzzles)
            {
                if (!puzzle.Connected) return;
            }
            
            _carData.SaveData.Passed = true;
            _secondCarData.SaveData.Passed = true;

            CarData carData, secondCarData;
            if (_carData.Level < ExpertModeStaticData.CurrentExpertLevelData.CarDatas.Length)
            {
                carData = ExpertModeStaticData.CurrentExpertLevelData.CarDatas[_carData.Level];
                secondCarData = ExpertModeStaticData.CurrentExpertLevelData.SecondCarDatas[_carData.Level];
                carData.SaveData.Locked = false;
                secondCarData.SaveData.Locked = false;

                DataHelper.SaveData(carData.name + ".data", carData.SaveData);
                DataHelper.SaveData(secondCarData.name + ".data", secondCarData.SaveData);
            }

            DataHelper.SaveData(_carData.name + ".data", _carData.SaveData);
            DataHelper.SaveData(_secondCarData.name + ".data", _secondCarData.SaveData);

            var currentLevel = GeneralStaticData.CurrentMasterLevelData;
            currentLevel.SaveData.Locked = false;
            DataHelper.SaveData(currentLevel.name + ".data", currentLevel.SaveData);

            StartCoroutine(Pause(1));
        }

        private IEnumerator Pause(int p)
        {
            yield return new WaitForSeconds(p);
            _onPuzzlesReady?.Invoke();
        }

        public override void SwitchToNextCar()
        {
            var carDatas = ExpertModeStaticData.CurrentExpertLevelData.CarDatas;
            var secondCarDatas = ExpertModeStaticData.CurrentExpertLevelData.SecondCarDatas;
            if (_carData.Level < carDatas.Length)
            {
                ExpertModeStaticData.CurrentTwoCarsData = new Tuple<CarData, CarData>(carDatas[_carData.Level], secondCarDatas[_carData.Level]);
            }
            else
            {
                ExpertModeStaticData.CurrentTwoCarsData = new Tuple<CarData, CarData>(carDatas[0], secondCarDatas[0]);
            }
        }
    }
}
