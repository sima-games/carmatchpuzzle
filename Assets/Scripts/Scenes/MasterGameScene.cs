﻿using CarConstruct.PuzzlePieces;
using CarConstruct.ScriptableObjects;
using CarConstruct.StaticData;
using CarConstruct.Truck;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;

namespace CarConstruct.Scenes
{
    public class MasterGameScene : GameScene
    {
        private CarData _secondCarData;
        private CarData _thirdCarData;
        [SerializeField] private GameObject _secondTruck;
        [SerializeField] private GameObject _thirdTruck;

        private static bool isPlayed = false;
        private void Start()
        {
            _puzzles = new List<PuzzlePiece>();

            _carData = MasterModeStaticData.CurrentThreeCarsData.Item1;
            _secondCarData = MasterModeStaticData.CurrentThreeCarsData.Item2;
            _thirdCarData = MasterModeStaticData.CurrentThreeCarsData.Item3;
            var index = 0;
            var arraysLength = _carData.PuzzlePieceDatas.Length + _secondCarData.PuzzlePieceDatas.Length + _thirdCarData.PuzzlePieceDatas.Length;
            foreach (var puzzle in _carData.PuzzlePieceDatas)
            {
                var puzzlePieceObject = Instantiate(_puzzlePiecePrefab);
                var puzzlePiece = puzzlePieceObject.GetComponent<PuzzlePiece>();
                puzzlePiece.InitPiece(_truck, arraysLength, _puzzleInitPos, index, this, puzzle, puzzle.Sprite);
                _puzzles.Add(puzzlePiece);
                index++;
            }

            foreach (var puzzle in _secondCarData.PuzzlePieceDatas)
            {
                var puzzlePieceObject = Instantiate(_puzzlePiecePrefab);
                var puzzlePiece = puzzlePieceObject.GetComponent<PuzzlePiece>();
                puzzlePiece.InitPiece(_secondTruck, arraysLength, _puzzleInitPos, index, this, puzzle, puzzle.Sprite);
                _puzzles.Add(puzzlePiece);
                index++;
            }

            foreach (var puzzle in _thirdCarData.PuzzlePieceDatas)
            {
                var puzzlePieceObject = Instantiate(_puzzlePiecePrefab);
                var puzzlePiece = puzzlePieceObject.GetComponent<PuzzlePiece>();
                puzzlePiece.InitPiece(_thirdTruck, arraysLength, _puzzleInitPos, index, this, puzzle, puzzle.Sprite);
                _puzzles.Add(puzzlePiece);
                index++;
            }

            if (!isPlayed)
            {
                isPlayed = true;
                _audio.PlayOneShot(_startAudio);
            }
        }

        public override void CheckPuzzles()
        {
            foreach (var puzzle in _puzzles)
            {
                if (!puzzle.Connected) return;
            }

            _carData.SaveData.Passed = true;
            _secondCarData.SaveData.Passed = true;
            _thirdCarData.SaveData.Passed = true;

            if (_carData.Level < MasterModeStaticData.CurrentMasterLevelData.CarDatas.Length)
            {
                var carData = MasterModeStaticData.CurrentMasterLevelData.CarDatas[_carData.Level];
                var secondCarData = MasterModeStaticData.CurrentMasterLevelData.SecondCarDatas[_carData.Level];
                var thirdCarData = MasterModeStaticData.CurrentMasterLevelData.ThirdCarDatas[_carData.Level];

                carData.SaveData.Locked = false;
                secondCarData.SaveData.Locked = false;
                thirdCarData.SaveData.Locked = false;
                
                DataHelper.SaveData(carData.name + ".data", carData.SaveData);
                DataHelper.SaveData(secondCarData.name + ".data", secondCarData.SaveData);
                DataHelper.SaveData(thirdCarData.name + ".data", thirdCarData.SaveData);
            }

            DataHelper.SaveData(_carData.name + ".data", _carData.SaveData);
            DataHelper.SaveData(_secondCarData.name + ".data", _secondCarData.SaveData);
            DataHelper.SaveData(_thirdCarData.name + ".data", _thirdCarData.SaveData);



            StartCoroutine(Pause(1));
        }

        private IEnumerator Pause(int p)
        {
            yield return new WaitForSeconds(p);
            _onPuzzlesReady?.Invoke();
        }
        public override void SwitchToNextCar()
        {
            var carDatas = MasterModeStaticData.CurrentMasterLevelData.CarDatas;
            var secondCarDatas = MasterModeStaticData.CurrentMasterLevelData.SecondCarDatas;
            var thirdCarDatas = MasterModeStaticData.CurrentMasterLevelData.ThirdCarDatas;
            if (_carData.Level < carDatas.Length)
            {
                MasterModeStaticData.CurrentThreeCarsData = new Tuple<CarData, CarData, CarData>(carDatas[_carData.Level], secondCarDatas[_carData.Level], thirdCarDatas[_carData.Level]);
            }
            else
            {
                MasterModeStaticData.CurrentThreeCarsData = new Tuple<CarData, CarData, CarData>(carDatas[0], secondCarDatas[0], thirdCarDatas[0]);
            }
        }
    }
}
