﻿using CarConstruct.PuzzlePieces;
using CarConstruct.ScriptableObjects;
using CarConstruct.StaticData;
using CarConstruct.Truck;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;

namespace CarConstruct.Scenes
{
    public class GameScene : MonoBehaviour
    {
        [SerializeField] protected Vector2 _puzzleInitPos;
        [SerializeField] protected UnityEvent _onPuzzlesReady;
        [SerializeField] protected UnityEvent _onLastCarInLevel;
        [SerializeField] protected GameObject _puzzlePiecePrefab;
        protected List<PuzzlePiece> _puzzles;
        protected CarData _carData;
        [SerializeField] protected GameObject _truck;
        [SerializeField] protected AudioSource _audio;
        [SerializeField] protected AudioClip _startAudio;

        private static bool isPlayed = false;
        private void Start()
        {
            _puzzles = new List<PuzzlePiece>();

            _carData = DefaultModeStaticData.CurrentCarData;
            var index = 0;
            foreach (var puzzle in _carData.PuzzlePieceDatas)
            {
                var puzzlePieceObject = Instantiate(_puzzlePiecePrefab);
                var puzzlePiece = puzzlePieceObject.GetComponent<PuzzlePiece>();
                puzzlePiece.InitPiece(_truck, 0, _puzzleInitPos, index, this, puzzle, puzzle.Sprite, false);
                _puzzles.Add(puzzlePiece);
                index++;
            }
            if (!isPlayed)
            {
                isPlayed = true;
                _audio.PlayOneShot(_startAudio);
            }
        }

        public virtual void CheckPuzzles()
        {
            foreach (var puzzle in _puzzles)
            {
                if (!puzzle.Connected) return;
            }

            _carData.SaveData.Locked = false;

            DataHelper.SaveData(_carData.name + ".data", _carData.SaveData);

            var currentLevel = GeneralStaticData.CurrentExpertLevelData;
            currentLevel.SaveData.Locked = false;
            DataHelper.SaveData(currentLevel.name + ".data", currentLevel.SaveData);

            StartCoroutine(Pause(1));
        }

        private IEnumerator Pause(int p)
        {
            yield return new WaitForSeconds(p);
            _onPuzzlesReady?.Invoke();
        }

        public virtual void SwitchToNextCar()
        {
            var carDatas = DefaultModeStaticData.CurrentLevelData.CarDatas;
            if (_carData.Level < carDatas.Length)
            {
                DefaultModeStaticData.CurrentCarData = carDatas[_carData.Level];
            } 
            else
            {
                DefaultModeStaticData.CurrentCarData = carDatas[0];
            }
        }
    }
}
