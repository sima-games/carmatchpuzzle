﻿using Assets.Scripts.Truck;
using CarConstruct.Events;
using CarConstruct.ScriptableObjects;
using CarConstruct.StaticData;
using CarConstruct.Truck;
using CarConstruct.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace CarConstruct.Scenes
{
    public class MenuScene : MonoBehaviour
    {
        [SerializeField] private UnityCarDataEvent _onLevelUpdated;
        [SerializeField] protected GameObject _levelTruckPrefab;
        [SerializeField] protected Canvas _canvas;
        private void Start()
        {
            Vector2 screenBounds = Camera.main.ScreenToWorldPoint(new Vector2(Screen.width, Screen.height));

            var currentLevel = DefaultModeStaticData.CurrentLevelData;
            var carDatas = currentLevel.CarDatas;

            var index = 0;
            foreach (var carData in carDatas)
            {
                var loadedData = DataHelper.ReadData<SaveableCarData>(carData.name + ".data");
                if (loadedData != null)
                {
                    carDatas[index].SaveData = loadedData;
                }

                var levelTruck = Instantiate(_levelTruckPrefab);
                levelTruck.transform.SetParent(_canvas.transform);
                levelTruck.transform.localScale = Vector3.one;

                var x = Constants.LevelTruckSpawnWidth / (3 * 2) + Constants.LevelTruckSpawnWidth / 3 * (index % 3) - Constants.LevelTruckSpawnWidth / 2;
                var y = Constants.LevelTruckSpawnHeight / (3 * 2) + Constants.LevelTruckSpawnHeight / 3 * (index / 3) - Constants.LevelTruckSpawnHeight / 2 - 2;
                levelTruck.transform.position = new Vector2(x, -y);

                var image = levelTruck.GetComponent<Image>();
                image.sprite = carData.ExampleCar;

                var star = levelTruck.transform.GetChild(0);
                star.gameObject.SetActive(!carData.SaveData.Locked);

                var script = levelTruck.GetComponent<LevelTruck>();
                script.Button.onClick.AddListener(() => _onLevelUpdated?.Invoke(carData));

                index++;
            }
        }
    }
}
