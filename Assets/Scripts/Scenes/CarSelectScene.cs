﻿using CarConstruct.ScriptableObjects;
using CarConstruct.StaticData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;

namespace Assets.Scripts.Scenes
{
    class CarSelectScene : MonoBehaviour
    {
        [SerializeField] private ExpertLevelData _currentExpertLevelData;
        [SerializeField] private MasterLevelData _currentMasterLevelData;
        [SerializeField] private GeneralStaticData _generalStaticData;
        [SerializeField] private UnityEvent _onExpertSelected;
        [SerializeField] private UnityEvent _onExpertCannotBeSelected;
        [SerializeField] private UnityEvent _onMasterSelected;
        [SerializeField] private UnityEvent _onMasterCannotBeSelected;
        public void SelectExpertLevel()
        {
            _generalStaticData.SetCurrentLevelData(_currentExpertLevelData);
            _generalStaticData.SetCurrentLevelData(_currentMasterLevelData);

            var level = GeneralStaticData.CurrentExpertLevelData;
            var loadedData = DataHelper.ReadData<SaveableLevelData>(level.name + ".data");

            if (loadedData != null)
            {
                level.SaveData = loadedData;
            }

            if (level.SaveData.Locked)
            {
                _onExpertCannotBeSelected?.Invoke();
            } 
            else
            {
                _onExpertSelected.Invoke();
            }
        }
        
        public void SelectMasterLevel()
        {
            _generalStaticData.SetCurrentLevelData(_currentExpertLevelData);
            _generalStaticData.SetCurrentLevelData(_currentMasterLevelData);

            var level = GeneralStaticData.CurrentMasterLevelData;
            var loadedData = DataHelper.ReadData<SaveableLevelData>(level.name + ".data");

            if (loadedData != null)
            {
                level.SaveData = loadedData;
            }

            if (level.SaveData.Locked)
            {
                _onMasterCannotBeSelected?.Invoke();
            }
            else
            {
                _onMasterSelected.Invoke();
            }
        }
    }
}
