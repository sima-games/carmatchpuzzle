﻿using CarConstruct.ScriptableObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine.Events;

namespace CarConstruct.Events
{
    [Serializable] public class UnityCarDataEvent : UnityEvent<CarData> { }
    [Serializable] public class UnityTwoCarsDataEvent : UnityEvent<Tuple<CarData, CarData>> { }
    [Serializable] public class UnityThreeCarsDataEvent : UnityEvent<Tuple<CarData, CarData, CarData>> { }
}
