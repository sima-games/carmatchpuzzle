﻿using CarConstruct.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;

namespace CarConstruct
{
    [Serializable]
    public class LevelSelector : MonoBehaviour
    {
        private int _currentLevelIndex = 0;

        [HideInInspector]
        public int NumberOfLevels = 0;

        [SerializeField] private UnityTupleEvent _onLevelSelected;

        public void SwitchLevel(bool next)
        {
            if (next)
            {
                if (_currentLevelIndex < NumberOfLevels - 1)
                {
                    _currentLevelIndex++;
                }
                else
                {
                    _currentLevelIndex = 0;
                }
            }
            else
            {
                if (_currentLevelIndex > 0)
                {
                    _currentLevelIndex--;
                }
                else
                {
                    _currentLevelIndex = NumberOfLevels - 1;
                }
            }

            _onLevelSelected.Invoke(new Tuple<int, bool>(_currentLevelIndex, next));
        }
    }
}
